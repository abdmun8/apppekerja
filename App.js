import React, { Component } from "react";
import { createAppContainer, createStackNavigator } from "react-navigation";
import HomeScreen from "./screens/HomeScreen";
import KalenderScreen from "./screens/KalenderScreen";
import CatatanScreen from "./screens/CatatanScreen";
import DetailCatatanScreen from "./screens/DetailCatatanScreen";
import EditCatatanScreen from "./screens/EditCatatanScreen";
import KalenderInfo from "./screens/KalenderInfo";
import NewPkbScreen from "./screens/NewPkbScreen";
import JadwalShiftScreen from "./screens/JadwalShiftScreen";


export default class App extends Component {  
  render() {
    return <AppContainer />;
  }
}

const AppStackNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Kalender: KalenderScreen,
    Catatan: CatatanScreen,
    PKB: NewPkbScreen,
    DetailCatatan: DetailCatatanScreen,
    JadwalShift: JadwalShiftScreen,
    KalenderInfo: KalenderInfo,
    EditCatatan: EditCatatanScreen
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: "#F64A1F"
      },
      title: "Aplikasi Pekerja",
      headerTintColor: "#fff"
    }
  }
);

const AppContainer = createAppContainer(AppStackNavigator);
