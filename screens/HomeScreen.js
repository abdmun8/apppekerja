import React, { Component } from "react";
import { TouchableOpacity, Image, StyleSheet } from "react-native";
import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  Icon,
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";

class HomeScreen extends Component {
  static navigationOptions = {
    title: "Home"
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content>
          <Grid style={{ padding: 30 }}>
            <Row size={100}>
              <Col style={styles.headerColStyle}>
                <Image
                  style={{ alignSelf: "center", width: 100, height: 100 }}
                  source={require("../images/logo.png")}
                />
                <Text>PT Indonesia Thai Summit Plastech</Text>
              </Col>
            </Row>
            <Row size={50}>
              <Col style={styles.colStyle}>
                <TouchableOpacity onPress={() => navigate("Kalender")}>
                  <Card style={styles.cardStyle}>
                    <Icon
                      type="Ionicons"
                      style={[styles.iconStyle, { color: "orange" }]}
                      name="calendar"
                    />
                    <Text style={styles.textStyle}>Kalender</Text>
                  </Card>
                </TouchableOpacity>
              </Col>
              <Col style={styles.colStyle}>
                <TouchableOpacity onPress={() => navigate("PKB")}>
                  <Card style={styles.cardStyle}>
                    <Icon
                      type="Ionicons"
                      style={[styles.iconStyle, { color: "#00b33c" }]}
                      name="book"
                    />
                    <Text style={styles.textStyle}>PKB</Text>
                  </Card>
                </TouchableOpacity>
              </Col>
            </Row>
            <Row size={50}>
              <Col style={styles.colStyle}>
                <TouchableOpacity onPress={() => navigate("JadwalShift")}>
                  <Card style={styles.cardStyle}>
                    <Icon
                      type="Ionicons"
                      style={[styles.iconStyle, { color: "#00e6e6" }]}
                      name="clock"
                    />
                    <Text style={styles.textStyle}>Jadwal Shift</Text>
                  </Card>
                </TouchableOpacity>
              </Col>
              <Col style={styles.colStyle}>
                <TouchableOpacity onPress={() => navigate("Catatan")}>
                  <Card style={styles.cardStyle}>
                    <Icon
                      type="Ionicons"
                      style={[styles.iconStyle, { color: "#cc00cc" }]}
                      name="bookmarks"
                    />
                    <Text style={styles.textStyle}>Buku Catatan</Text>
                  </Card>
                </TouchableOpacity>
              </Col>
            </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  colStyle: {
    height: "auto",
    padding: 10
  },
  iconStyle: {
    fontSize: 60,
    alignSelf: "center",
    flex: 1,
    marginBottom: 5
  },
  cardStyle: {
    width: "auto",
    borderRadius: 20,
    padding: 15
  },
  headerColStyle: {
    height: 200,
    alignItems: "center",
    justifyContent: "center"
  },
  textStyle: {
    textAlign: "center",
    fontSize: 12
  }
});

export default HomeScreen;
