import React, { Component } from "react";
import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  Icon,
  Right
} from "native-base";
export default class KalenderInfo extends Component {
  static navigationOptions = {
    title: "Informasi"
  };
  render() {
    return (
      <Container>
        <Content>
          <Card>
            <CardItem>
              <Icon
                active
                name="information-circle"
                style={{ color: "#ff1a1a" }}
              />
              <Text>Libur Nasional</Text>
            </CardItem>
            <CardItem>
              <Icon
                active
                name="information-circle"
                style={{ color: "#0000ff" }}
              />
              <Text>Cuti Bersama</Text>
            </CardItem>
            <CardItem>
              <Icon
                active
                name="information-circle"
                style={{ color: "#1C8B35" }}
              />
              <Text>Libur Perusahaan</Text>
            </CardItem>
            <CardItem>
              <Icon
                active
                name="information-circle"
                style={{ color: "#ffff1a" }}
              />
              <Text>Tukar Off</Text>
            </CardItem>
            <CardItem>
              <Icon
                active
                name="information-circle"
                style={{ color: "#d633ff" }}
              />
              <Text>Sabtu Masuk (Hari Pengganti)</Text>
            </CardItem>
            <CardItem>
              <Icon
                active
                name="information-circle"
                style={{ color: "#e63900" }}
              />
              <Text>Masuk hanya shift malam</Text>
            </CardItem>
            <CardItem>
              <Icon
                active
                name="information-circle"
                style={{ color: "#ff751a" }}
              />
              <Text>Masuk hanya shift siang & office</Text>
            </CardItem>
            <CardItem>
              <Icon
                active
                name="information-circle"
                style={{ color: "#00e6e6" }}
              />
              <Text>Sabtu Masuk (Pengganti Libur Perusahaan)</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
