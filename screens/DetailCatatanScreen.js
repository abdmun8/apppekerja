import React, { Component } from "react";
import {
  Container,
  Fab,
  Content,
  Card,
  CardItem,
  Text,
  Body
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { withNavigation } from "react-navigation";

var SQLite = require("react-native-sqlite-storage");
var db = SQLite.openDatabase({
  name: "tsapp.db",
  createFromLocation: "~tsapp.db"
});
class DetailCatatanScreen extends Component {
  static navigationOptions = {
    title: "Detail Catatan"
  };

  constructor(props) {
    super(props);
    this.state = {
      id: "",
      judul: "",
      isi: "",
      isNew: false
    };
  }

  componentWillMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      this.setState({ id: navigation.state.params.note.id });
      this.setState({ judul: navigation.state.params.note.judul });
      this.setState({ isi: navigation.state.params.note.isi });
      this.getNoteDetail();
    });
  }

  getNoteDetail() {
    db.transaction(tx => {
      tx.executeSql(
        "SELECT * FROM note where id=?",
        [this.state.id],
        (tx, results) => {
          var len = results.rows.length;
          var notes = {};
          if (len > 0) {
            for (let i = 0; i < len; i++) {
              let row = results.rows.item(i);
              notes.id = row.id;
              notes.judul = row.judul;
              notes.isi = row.isi;
            }
            this.setState({ id: notes.id });
            this.setState({ judul: notes.judul });
            this.setState({ isi: notes.isi });
          }
        }
      );
    });
  }

  renderDetail() {
    return (
      <Content padder>
        <Card>
          <CardItem header bordered>
            <Text style={{ color: "black", fontSize: 20 }}>
              {this.state.judul}
            </Text>
          </CardItem>
          <CardItem bordered>
            <Body>
              <Text>{this.state.isi}</Text>
            </Body>
          </CardItem>
        </Card>
      </Content>
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        {this.renderDetail()}
        <Fab
          active={1}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: "#1C8B35" }}
          position="bottomRight"
          onPress={() =>
            navigate("EditCatatan", [
              { noteId: this.state.id },
              { judul: this.state.judul },
              { isi: this.state.isi },
              { isNew: this.state.isNew }
            ])
          }
        >
          {/* <Icon theme={{ iconFamily: 'FontAwesome' }} name="fa-pencil" /> */}
          <Icon name="pencil" />
        </Fab>
      </Container>
    );
  }
}

export default withNavigation(DetailCatatanScreen);
