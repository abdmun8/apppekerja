import React, { Component } from "react";
import {
  Container,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Fab,
  Icon
} from "native-base";
import { withNavigation } from "react-navigation";

var SQLite = require("react-native-sqlite-storage");
var db = SQLite.openDatabase({name: 'tsapp.db', createFromLocation: '~tsapp.db'});
class CatatanScreen extends Component {
  static navigationOptions = {
    title: "Catatan"
  };

  constructor(props) {
    super(props);   

    this.state = {
      notes: [],
      active: "true"
    };
  }

  componentWillMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      this.getNotes()
    });
  }

  getNotes(){
    db.transaction(tx => {
      tx.executeSql("SELECT * FROM note", [], (tx, results) => {
        var len = results.rows.length;
        var notes = [];
        if (len > 0) {
          for (let i = 0; i < len; i++) {
            let row = results.rows.item(i);
            const { id, judul, isi } = row;
            notes.push({ id, judul, isi });
          }
          this.setState({ notes: notes });
        }
      });
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content>
          <List>{this.renderItems()}</List>
        </Content>
        <Fab
          active={1}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: "#1C8B35" }}
          position="bottomRight"
          onPress={() =>
            navigate("EditCatatan", [{noteId:''},{judul:''},{isi:''},{isNew:true}])
          }
        >
          <Icon type="Ionicons" name="add" />
        </Fab>
      </Container>
    );
  }

  renderItems() {
    const { navigate } = this.props.navigation;
    return this.state.notes.map(note => {
      let url = require("../images/logo.png");
      return (
        <ListItem
          thumbnail
          key={note.id}
          button
          onPress={() => navigate("DetailCatatan", { note })}
        >
          <Left>
            <Thumbnail square source={url} />
          </Left>
          <Body>
            <Text>{note.judul}</Text>
            <Text note numberOfLines={2}>
              {note.isi.substring(0, 60) + ". ."}
            </Text>
          </Body>
        </ListItem>
      );
    });
  }
}

export default withNavigation(CatatanScreen);
