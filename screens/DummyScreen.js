/* @flow weak */

import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ToastAndroid,
  ScrollView
} from 'react-native';
// import { openDatabase } from 'react-native-sqlite-storage';

var SQLite = require('react-native-sqlite-storage');
var db = SQLite.openDatabase({name: 'apppekerja.db', createFromLocation: '~apppekerja.db'});
const notes = [];

export default class DummyScreen extends React.Component{
  constructor(props) {
    super(props);
    this.state = {notes:[
      {'id':1,judul:'test1',isi:'ini isi text1'},
      {'id':2,judul:'test2',isi:'ini isi text2'},
      {'id':3,judul:'test3',isi:'ini isi text3'},
      {'id':4,judul:'test4',isi:'ini isi text4'},
    ]};

    // db.transaction((tx) => {
    //   tx.executeSql('SELECT * FROM note', [], (tx, results) => {
    //       var len = results.rows.length;
    //       console.log(len)
    //       if(len > 0) {
    //         // exists owner name John
    //         // var row = results.rows.item.row(1);
    //         // this.setState({notes: row}); 
    //         // console.log(row)
    //       }
    //     });
    // });

    // ToastAndroid.show('Hello!!', ToastAndroid.SHORT);
    
  }

  renderNotes(){
    return this.state.notes.map( note => <Text key={note.id}>{note.judul}</Text>)
  }

  render(){
    // console.log(this.state)
      return (
        // this.listProduct()
          // <View style={styles.container}>
          <ScrollView>
              {this.renderNotes()}
              {/* <Text>I'm DummyScreen</Text>
              <Text>{'Mary \'s pet is ' + this.state.judul}</Text> */}
          </ScrollView>
          // </View>
          
    )
  }

  listProduct(){
    
    return (
      this.state.notes.map( note => {
        <View>
          <Text>{note.judul}</Text>
          <Text>{note.isi}</Text>
          </View>}
      )
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
