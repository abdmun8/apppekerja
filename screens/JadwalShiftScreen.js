import React, { Component } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import Pdf from "react-native-pdf";

class JadwalShiftScreen extends Component {
  static navigationOptions = {
    title: "Jadwal Shift"
  };

  render() {
    let yourPDFURI = { uri: "bundle-assets://pdf/jadwalPdf.pdf", cache: true };
    return (
      <View style={styles.container}>
        <Pdf
          ref={pdf => {
            this.pdf = pdf;
          }}
          source={yourPDFURI}
          style={styles.pdf}
          onError={error => {
            console.log(error);
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 25
  },
  pdf: {
    flex: 1,
    width: Dimensions.get("window").width
  }
});

export default JadwalShiftScreen;
