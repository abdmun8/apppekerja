import React, { Component } from "react";
import { Calendar, LocaleConfig } from "react-native-calendars";
import Table from "react-native-simple-table";
import { Col, Row, Grid } from "react-native-easy-grid";
import { withNavigation } from "react-navigation";
import {
  Button,
  View,
  Text,
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Form,
  Item,
  Input,
  Label,
  Left
} from "native-base";
import Modal from "react-native-modal";
import { ToastAndroid } from "react-native";
var SQLite = require("react-native-sqlite-storage");
var db = SQLite.openDatabase({
  name: "tsapp.db",
  createFromLocation: "~tsapp.db"
});

LocaleConfig.locales["id"] = {
  monthNames: [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni ",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"
  ],
  monthNamesShort: [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "Mei",
    "Jun ",
    "Jul",
    "Agu",
    "Sep",
    "Okt",
    "Nov",
    "Des"
  ],
  dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"],
  dayNamesShort: ["Min.", "Sen.", "Sel.", "Rab.", "Kam.", "Jum.", "Sab."]
};

LocaleConfig.defaultLocale = "id";

class KalenderScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      monthString: "",
      startDate: "",
      endDate: "",
      lemburToday: 0,
      tglLemburToday: "",
      dataLembur: [],
      keterangan: [],
      modalVisible: false,
      markedDates: {}
    };
  }

  getStartdate(dt = null) {
    var MyDate = dt == null ? new Date() : new Date(dt);
    return (
      MyDate.getFullYear() +
      "-" +
      ("0" + (MyDate.getMonth() + 1)).slice(-2) +
      "-01"
    );
  }

  getEndDate(dt = null) {
    var MyDate = dt == null ? new Date() : new Date(dt);
    var l = this.lastDay(MyDate.getFullYear(), MyDate.getMonth());
    return (
      MyDate.getFullYear() +
      "-" +
      ("0" + (MyDate.getMonth() + 1)).slice(-2) +
      "-" +
      l
    );
  }

  lastDay(y, m) {
    return new Date(y, m + 1, 0).getDate();
  }

  getMonthString() {
    var MyDate = new Date();
    MyDate.setDate(MyDate.getDate() + 20);
    return ("0" + (MyDate.getMonth() + 1)).slice(-2);
  }

  componentWillMount() {
    var s = this.getStartdate();
    var e = this.getEndDate();
    this.setState({ startDate: s });
    this.setState({ endDate: e });

    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {});
  }

  getKalenderData() {
    db.transaction(tx => {
      tx.executeSql(
        "SELECT * FROM company_kalender UNION select tanggal, tanggal, 1 as ket, 1 as warna from lembur where total > 0",
        [],
        (tx, results) => {
          var len = results.rows.length;
          var kalender = [];
          if (len > 0) {
            for (let i = 0; i < len; i++) {
              let row = results.rows.item(i);
              const { tanggal, ket, warna } = row;
              kalender.push({ tanggal, ket, warna });
            }

            let newDaysObject = {};
            kalender.forEach(day => {
              newDaysObject = {
                ...newDaysObject,
                [day.tanggal]:
                  day.ket != 1
                    ? { selected: true, selectedColor: day.warna }
                    : { marked: true }
              };
            });

            this.setState({ markedDates: newDaysObject });
          }
        }
      );
    });
  }

  componentDidMount() {
    this.getKalenderData();
    var s = this.getStartdate();
    var e = this.getEndDate();
    this.getLemburPerMonth(s, e);
    this.getKalenderPerMonth(s, e);
  }

  getLemburPerMonth(start, end) {
    db.transaction(tx => {
      tx.executeSql(
        "SELECT * FROM lembur where tanggal >= ? and tanggal <= ? and total > 0",
        [start, end],
        (tx, results) => {
          var len = results.rows.length;
          var lembur = [];
          var tot = 0;
          if (len > 0) {
            for (let i = 0; i < len; i++) {
              let row = results.rows.item(i);
              const { tanggal, total } = row;
              tot += total;
              lembur.push({ tgl: tanggal, total: total });
            }
          }
          lembur.push({ tgl: "Total", total: tot });
          this.setState({ dataLembur: lembur });
        }
      );
    });
  }

  getKalenderPerMonth(start, end) {
    db.transaction(tx => {
      tx.executeSql(
        "SELECT * FROM company_kalender where tanggal >= ? and tanggal <= ?",
        [start, end],
        (tx, results) => {
          var len = results.rows.length;
          var keterangan = [];
          if (len > 0) {
            for (let i = 0; i < len; i++) {
              let row = results.rows.item(i);
              const { tanggal, ket } = row;
              keterangan.push({ tanggal, ket });
            }
          }
          this.setState({ keterangan: keterangan });
          console.log(this.state.keterangan);
        }
      );
    });
  }

  saveLembur() {
    console.log(this.state);
    db.transaction(tx => {
      tx.executeSql(
        "REPLACE INTO lembur (tanggal,total) VALUES (?,?)",
        [this.state.tglLemburToday, this.state.lemburToday],
        (tx, results) => {
          console.log("Results", results.rowsAffected);
          // if (results.rowsAffected > 0) {
          //   ToastAndroid.show("Lembur disimpan!", ToastAndroid.SHORT);
          // }
          this.getKalenderData();
          ToastAndroid.show("Lembur disimpan!", ToastAndroid.SHORT);
        }
      );
    });
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: "Kalender",
      headerRight: (
        <Icon
          type="Ionicons"
          name="information-circle"
          style={{ marginRight: 20, color: "#fff" }}
          button
          onPress={() => navigation.navigate("KalenderInfo")}
        />
      )
    };
  };

  setDateState(dt) {
    let stdt = this.getStartdate(dt.dateString);
    let endt = this.getEndDate(dt.dateString);
    this.setState({ startDate: stdt });
    this.setState({ endDate: endt });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content>
          <Calendar
            onMonthChange={dm => {
              let stdt = this.getStartdate(dm.dateString);
              let endt = this.getEndDate(dm.dateString);
              this.getLemburPerMonth(stdt, endt);
              this.getKalenderPerMonth(stdt, endt);
            }}
            onDayLongPress={day => {
              this.getLemburToday(day.dateString);
              this.setModalVisible(true);
            }}
            markedDates={this.state.markedDates}
            firstDay={1}
          />
          <Card transparent>
            <CardItem>
              <View padder>{this.renderKeterangan()}</View>
            </CardItem>

            <CardItem>{this.renderTable()}</CardItem>
          </Card>
          <View>{this.renderModal()}</View>
        </Content>
      </Container>
    );
  }

  getLemburToday(dt) {
    db.transaction(tx => {
      tx.executeSql(
        "SELECT * FROM lembur where tanggal = ?",
        [dt],
        (tx, results) => {
          var len = results.rows.length;
          var total = 0;
          if (len > 0) {
            for (let i = 0; i < len; i++) {
              let row = results.rows.item(i);
              total = row.total;
              id = row.id;
            }
          }
          this.setState({ lemburToday: total });
          this.setState({ tglLemburToday: dt });
          // this.setState({ idLemburToday: id });
          console.log(this.state);
        }
      );
    });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  renderModal() {
    return (
      <Modal
        isVisible={this.state.modalVisible}
        onSwipeComplete={() => this.setModalVisible(false)}
        swipeDirection="down"
      >
        <View
          style={{
            height: 240,
            borderRadius: 10,
            backgroundColor: "white",
            padding: 20
          }}
        >
          <Grid>
            <Row>
              <Col>
                <Text
                  style={{
                    fontSize: 20,
                    textAlign: "center",
                    fontWeight: "bold"
                  }}
                >
                  Input Lembur
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    textAlign: "center",
                    fontWeight: "normal"
                  }}
                >
                  {this.state.tglLemburToday}
                </Text>
                <Form>
                  <Item>
                    <Label>Total Jam</Label>
                    <Input
                      onChangeText={text => {
                        this.setState({ lemburToday: text });
                      }}
                    >
                      {this.state.lemburToday}
                    </Input>
                  </Item>
                </Form>
              </Col>
            </Row>
            <Row style={{ height: 60 }}>
              <Col>
                <Button
                  rounded
                  style={{ alignSelf: "center" }}
                  light
                  iconLeft
                  onPress={() => {
                    this.saveLembur();
                    this.setModalVisible(false);
                    let stdt = this.getStartdate(this.state.tglLemburToday);
                    let endt = this.getEndDate(this.state.tglLemburToday);
                    this.getLemburPerMonth(stdt, endt);
                  }}
                >
                  <Icon name="add" />
                  <Text>Tambah / Edit</Text>
                </Button>
              </Col>
              <Col>
                <Button
                  rounded
                  style={{ alignSelf: "center" }}
                  danger
                  iconLeft
                  onPress={() => {
                    this.setState({ lemburToday: 0 });
                    this.saveLembur();
                    this.setModalVisible(false);
                    let stdt = this.getStartdate(this.state.tglLemburToday);
                    let endt = this.getEndDate(this.state.tglLemburToday);
                    this.getLemburPerMonth(stdt, endt);
                  }}
                >
                  <Icon name="trash" />
                  <Text>Hapus</Text>
                </Button>
              </Col>
            </Row>
          </Grid>
        </View>
      </Modal>
    );
  }

  renderTable() {
    let columns = [
      {
        title: "Tanggal",
        dataIndex: "tgl",
        width: 150
      },
      {
        title: "Jumlah jam",
        dataIndex: "total",
        width: 150
      }
    ];

    let dataSource = [
      { tgl: "2019-05-02", jml: "10 jam" },
      { tgl: "2019-05-07", jml: "12 jam" },
      { tgl: "Total", jml: "22 jam" }
    ];
    return (
      <View padder>
        <Table columns={columns} dataSource={this.state.dataLembur} />
      </View>
    );
  }

  renderKeterangan() {
    return this.state.keterangan.map(keterangan => {
      let tgl = keterangan.tanggal.substring(
        keterangan.tanggal.length - 2,
        keterangan.tanggal.length
      );
      return (
        <Text key={tgl + keterangan.ket}>
          {Number(tgl)} - {keterangan.ket}
        </Text>
      );
    });
  }
}

export default withNavigation(KalenderScreen);
