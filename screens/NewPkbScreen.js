import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Icon,
  Accordion,
  Text,
  View,
  Right
} from "native-base";
import { StyleSheet } from "react-native";

var SQLite = require("react-native-sqlite-storage");
var db = SQLite.openDatabase({
  name: "tsapp.db",
  createFromLocation: "~tsapp.db"
});

export default class NewPkbScreen extends Component {
  static navigationOptions = {
    title: "PKB"
  };

  constructor(props) {
    super(props);

    this.state = {
      pkb: []
    };
  }

  componentWillMount() {
    this.getPkb();
  }

  getPkb() {
    db.transaction(tx => {
      tx.executeSql("SELECT * FROM pkb order by urutan", [], (tx, results) => {
        var len = results.rows.length;
        var pkb = [];
        if (len > 0) {
          for (let i = 0; i < len; i++) {
            let row = results.rows.item(i);
            const {
              id,
              judul,
              isi,
              header_bab,
              title_bab,
              urutan,
              pasal
            } = row;
            pkb.push({ id, judul, isi, header_bab, title_bab, urutan, pasal });
          }
          this.setState({ pkb: pkb });
        }
      });
    });
  }

  _renderHeader(item, expanded) {
    return (
      <View
        style={{
          flexDirection: "row",
          padding: 10,
          justifyContent: "space-between",
          alignItems: "center",
          backgroundColor: "#eee"
        }}
      >
        <Text style={{ fontWeight: "bold" }}> {item.judul}</Text>
        {item.header_bab ? (
          <Right style={{ marginRight: 20 }}>
            <Text>{item.title_bab}</Text>
          </Right>
        ) : (
          <Text />
        )}
        {expanded ? (
          <Icon style={{ fontSize: 18 }} name="remove-circle" />
        ) : (
          <Icon style={{ fontSize: 18 }} name="add-circle" />
        )}
      </View>
    );
  }
  _renderContent(item) {
    return (
      <View style={{ padding: 10 }}>
        <Text style={{ fontWeight: "bold", marginBottom: 5 }}>
          {item.pasal}
        </Text>
        <Text
          style={{
            backgroundColor: "#fff"
          }}
        >
          {item.isi}
        </Text>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <Content padder style={{ backgroundColor: "white" }}>
          <Text style={styles.titleStyle}>Peraturan Perusahaan</Text>
          <Text style={[styles.titleStyle,{marginBottom:10}]}>2018 - 2020</Text>
          <Accordion
            dataArray={this.state.pkb}
            animation={true}
            expanded={true}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
          />
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  titleStyle: { textAlign: "center", fontWeight: "bold", fontSize: 20 }
});
