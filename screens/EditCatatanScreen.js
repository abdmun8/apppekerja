import React, { Component } from "react";
import { ToastAndroid } from "react-native";
import {
  Container,
  Fab,
  Content,
  Card,
  CardItem,
  Form,
  Item,
  Input,
  Textarea
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { withNavigation } from "react-navigation";

var SQLite = require("react-native-sqlite-storage");
var db = SQLite.openDatabase({
  name: "tsapp.db",
  createFromLocation: "~tsapp.db"
});
class EditCatatanScreen extends Component {
  static navigationOptions = {
    title: "Tambah/Edit Catatan"
  };

  constructor(props) {
    super(props);
    this.state = {
      id: "",
      judul: "",
      isi: "",
      isNew: false
    };
  }

  componentWillMount() {
    const { navigation } = this.props;
    console.log(navigation.state.params);
    this.focusListener = navigation.addListener("didFocus", () => {
      this.setState({ id: navigation.state.params[0].noteId });
      this.setState({ judul: navigation.state.params[1].judul });
      this.setState({ isi: navigation.state.params[2].isi });
      this.setState({ isNew: navigation.state.params[3].isNew });
    });
  }

  insertNotes() {
    db.transaction(tx => {
      tx.executeSql(
        "INSERT INTO note (judul, isi) VALUES (?,?)",
        [this.state.judul, this.state.isi],
        (tx, results) => {
          console.log("Results", results.rowsAffected);
          if (results.rowsAffected > 0) {
            ToastAndroid.show("Catatan Tersimpan!", ToastAndroid.SHORT);
          }
        }
      );
    });
  }

  updateNotes() {
    db.transaction(tx => {
      tx.executeSql(
        "UPDATE note set judul=?,isi=? WHERE id=?",
        [this.state.judul, this.state.isi, this.state.id],
        (tx, results) => {
          console.log("Results", results.rowsAffected);
          if (results.rowsAffected > 0) {
            ToastAndroid.show("Catatan Tersimpan!", ToastAndroid.SHORT);
          }
        }
      );
    });
  }

  renderDetail() {
    return (
      <Card transparent>
        <CardItem header>
          <Item>
            <Input
              rounded
              onChangeText={text => this.setState({ judul: text })}
              placeholder="Judul"
            >
              {this.state.judul}
            </Input>
          </Item>
        </CardItem>
        <CardItem>
          <Form>
            <Textarea
              rowSpan={10}
              placeholder="isi Catatan"
              onChangeText={text => this.setState({ isi: text })}
              value={this.state.isi}
            />
          </Form>
        </CardItem>
      </Card>
    );
  }

  render() {
    console.log(this.state);
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content padder>{this.renderDetail()}</Content>
        <Fab
          active={1}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: "#1C8B35" }}
          position="bottomRight"
          onPress={() => {
            if (this.state.isNew == true) {
              this.insertNotes();
            } else {
              this.updateNotes();
            }
          }}
        >
          <Icon name="save" />
        </Fab>
      </Container>
    );
  }
}

export default withNavigation(EditCatatanScreen);
